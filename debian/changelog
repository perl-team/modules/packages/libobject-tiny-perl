libobject-tiny-perl (1.09-2) UNRELEASED; urgency=medium

  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:43:18 +0100

libobject-tiny-perl (1.09-1) unstable; urgency=medium

  * New upstream release.
  * Add debian/upstream/metadata.
  * Install new CONTRIBUTING file.
  * debian/copyright: update Upstream-Contact.
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Wed, 21 Nov 2018 19:33:28 +0100

libobject-tiny-perl (1.08-1) unstable; urgency=medium

  [ Jonathan Yu ]
  * New upstream release

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Apr 2018 21:09:42 +0200

libobject-tiny-perl (1.06-1) unstable; urgency=low

  * Initial Release (Closes: #619079)

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 20 Mar 2011 20:57:11 -0400
